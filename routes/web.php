<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('layouts.app');
});

Auth::routes();

Route::group(['prefix' => 'gestion','middleware' => ['login']], function() {
    Route::resource('proyecto', 'ProyectoController'); 
    Route::delete('proyecto/reactivar/{id}', 'ProyectoController@reactivar');
    Route::get('proyecto/pago/{id}', 'ProyectoController@pago');
    Route::post('proyecto/factura', 'ProyectoController@factura');
});

Route::get('auth/logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');
