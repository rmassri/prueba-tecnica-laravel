<div class="sidebar" data-color="azure" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--    Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

            Tip 2: you can also add an image using data-image tag  
    -->
    
    <div class="logo">
        <img width="200" src="{{{ URL::asset('img/logo.png') }}}">
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link">
                    <p>Menu</p>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a href="{{ asset('gestion/proyecto')}}" class="nav-link" id="navbarDropdownMenuLink">
                    <p>Proyectos</p>
                </a>
            </li>
            <li class="nav-item dropdown">
            <a href="{{ asset('auth/logout') }}" class="nav-link" id="navbarDropdownMenuLink">Salir</a>
            </li>
        </ul>
    </div>
</div>