@extends('layouts.app')
@section('content')
    <div class="panel panel-primary panel-table">
        <div class="panel-heading">
            <h3 class="m-t-0 m-b-5">
                <i class="fa fa-folder-open-o"></i> Proyectos
            </h3>
        </div>
        <div class="panel-body">
            <a  href="/gestion/proyecto/create" class="btn btn-success float-right">
                <li class="fa fa-plus"></li>
                Agregar Proyecto
            </a>
            <br>
            <br>
            <br>
            <div class="table-responsive">
                <table class="border-line table table-condensed table-striped table-hover table-striped dataTable" id="content">
                    <thead>
                        <tr class="bg-primary">
                            <th>Nombre</th>
                            <th>Monto</th>
                            <th>Cuotas</th>
                            <th>Activo</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($proyecto as $proyecto)
                            <tr>
                                <td>
                                    {{ $proyecto->nombre }}
                                </td>
                                 <td>
                                    {{ $proyecto->monto }}
                                </td>
                                 <td>
                                    {{ $proyecto->cuotas }}
                                </td>
                                <td>
                                    @if ($proyecto->deleted_at == '' or $proyecto->deleted_at == null)
                                        <span class="label label-success">Activo</span>
                                    @else
                                        <span class="label label-danger">Inactivo</span>
                                    @endif
                                </td>
                                <td>
                                @if ($proyecto->proyectoPgo->sum('monto') < $proyecto->monto)
                                    <a href="/gestion/proyecto/pago/{{$proyecto->id}}" class="btn btn-default"><i class="fa fa-edit"></i> Pagar </a>
                                    @else
                                        <div class="btn btn-default"><i></i> Realizado </div>
                                    @endif
                                    &nbsp;&nbsp;
                                    <a href="/gestion/proyecto/{{ $proyecto->id }}" class="fa-fa-search"><i class="fa fa-search"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="/gestion/proyecto/{{$proyecto->id}}/edit" class="fa-fa-pencil"><i class="fa fa-pencil"></i></a>
                                    &nbsp;&nbsp;

                                    @if ($proyecto->deleted_at == '' or $proyecto->deleted_at == null)
                                        <a id="eliminar-campus" title="Eliminar" onclick="eliminarProyecto(<?php echo $proyecto->id;?>)" class="fa-fa-search" id="edit-campus"><i class="fa fa-trash"></i></a>
                                    @else
                                        <a id="eliminar-campus" title="Reactivar" onclick="reactvarProyecto(<?php echo $proyecto->id;?>)" class="fa-fa-search" id="edit-campus"><i class="fa fa-check"></i></a>
                                    @endif
                                    

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Nombre</th>
                            <th>Monto</th>
                            <th>Cuotas</th>
                            <th>Activo</th>
                            <th>Acción</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <br>
            <a  href="/gestion/proyecto/create" class="btn btn-success float-right">
                <li class="fa fa-plus"></li>
                Agregar Proyecto
            </a>
        </div>
    </div>

@endsection
@section('scripts')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $('#content').DataTable({
      "ordering": false,
      "language":{
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }
    });

    function eliminarProyecto(id){
        var proyecto_id = id;
      swal({
        title: '',
        text: "¿Está seguro de guardar los cambios?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, estoy seguro',
        cancelButtonText: 'Cancelar'
      }).then(function (result) {
        if (result.value) {
            axios.delete('/gestion/proyecto/'+proyecto_id,
            {
              proyecto_id : proyecto_id,
            }).then(function (response) {
              swal("Perfecto","Se ha eliminado con exito el proyecto","success");
              window.location.href = "/gestion/proyecto";
            }).catch(data => {
              console.log(data.response);
            });
        }
    });
    }

    function reactvarProyecto(id){
        var proyecto_id = id;
      swal({
        title: '',
        text: "¿Está seguro de guardar los cambios?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, estoy seguro',
        cancelButtonText: 'Cancelar'
      }).then(function (result) {
        if (result.value) {
            axios.delete('/gestion/proyecto/reactivar/'+proyecto_id,
            {
              proyecto_id : proyecto_id,
            }).then(function (response) {
              swal("Perfecto","Se ha eliminado con exito el proyecto","success");
              window.location.href = "/gestion/proyecto";
            }).catch(data => {
              console.log(data.response);
            });
        }
    });
    }
    </script>
@stop
