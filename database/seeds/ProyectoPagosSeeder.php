<?php

use Illuminate\Database\Seeder;

class ProyectoPagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('proyecto_pagos')->truncate();
        \DB::table('proyecto_pagos')->insert(
            [
                0 => [ "proyecto_id" => 1, "cuota" => 1, "fecha" => "2015-10-05", "monto" => 93704 ],
                1 => [ "proyecto_id" => 1, "cuota" => 2, "fecha" => "2015-11-05", "monto" => 61200 ],
                2 => [ "proyecto_id" => 1, "cuota" => 3, "fecha" => "2015-12-05", "monto" => 61200 ],
                3 => [ "proyecto_id" => 1, "cuota" => 4, "fecha" => "2016-01-05", "monto" => 61200 ],
                4 => [ "proyecto_id" => 1, "cuota" => 5, "fecha" => "2016-02-05", "monto" => 61200 ],
                5 => [ "proyecto_id" => 1, "cuota" => 6, "fecha" => "2016-03-05", "monto" => 61200 ],
                6 => [ "proyecto_id" => 1, "cuota" => 7, "fecha" => "2016-04-05", "monto" => 61200 ],
                7 => [ "proyecto_id" => 1, "cuota" => 8, "fecha" => "2016-05-05", "monto" => 61200 ],
                8 => [ "proyecto_id" => 1, "cuota" => 9, "fecha" => "2016-06-05", "monto" => 61200 ],
                9 => [ "proyecto_id" => 1, "cuota" => 10, "fecha" => "2016-07-05", "monto" => 61200 ],
                10 => [ "proyecto_id" => 1, "cuota" => 11, "fecha" => "2016-08-05", "monto" => 61200 ],
                11 => [ "proyecto_id" => 1, "cuota" => 12, "fecha" => "2016-09-05", "monto" => 3461200 ],
            ]
        );
    }
}
