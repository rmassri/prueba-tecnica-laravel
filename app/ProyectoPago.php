<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectoPago extends Model
{

	protected $fillable = [
    	'proyecto_id', 'cuota', 'fecha','monto',
    ];
    //
}
