<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proyecto;
use App\ProyectoPago;

use Auth;
use Validator;
use DB;
use Mail;
use App\Jobs\SendVerificationEmail;
class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$proyecto = Proyecto::get();

        $proyecto = Proyecto::select('proyectos.id','proyectos.nombre','proyectos.monto','proyectos.cuotas')->with('proyectoPgo')->get();
        //dd($proyectoPago);

        return view("proyecto.proyecto",compact('proyecto'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("proyecto.formStoreProyecto");
    }

    public function pago($id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->monto="";
        $proyectoPago = Proyecto::select('proyectos.id','proyectos.nombre')->where('proyectos.id',$id)->with('proyectoPgo')->get();
        if($proyectoPago->count()>0){
            if($proyectoPago[0]->proyectoPgo->count()>0){
                $proyecto->cuota=$proyectoPago[0]->proyectoPgo->count();
            }else{
                $proyecto->cuota=1;
            }
        }
        return view("proyecto.formStoreProyectoPago",compact('proyecto','proyectoPago'));
    }

    public function factura(Request $request)
    {
        $user=Auth::user();
        DB::beginTransaction();
        $data=$request['data'];
        $validator = Validator::make($request['data'], [
            'proyecto_id' => 'required' ,
            'cuota' => 'required|numeric',
            'fecha' => 'required',
            'monto' => 'required|numeric',
        ]);
        if(count($validator->messages())>0){
            $poyecto = [
                'message'=>$validator->messages(),
                'statusCode'=>'error',
                'proyectoPago'=>[],
                'cuota'=>$request['data']['cuota']
            ];
        }else{
            $extract=explode('T',$request['data']['fecha']);
            $data['fecha']=$extract[0];
            $poyecto = new ProyectoPago($data);
            $poyecto->save();
            $proyectoPago = Proyecto::select('proyectos.id','proyectos.nombre','proyectos.monto')->where('proyectos.id',$request['data']['proyecto_id'])->with('proyectoPgo')->get();
            if($proyectoPago[0]->proyectoPgo->sum('monto') > $proyectoPago[0]->monto){
                $poyecto = [
                'message'=>$validator->messages(),
                'statusCode'=>'errorMonto',
                'proyectoPago'=>$proyectoPago,
                'cuota'=>$request['data']['cuota']
            ];
            }else{
                dispatch(new SendVerificationEmail($user));
                DB::commit();
                $poyecto = [
                'message'=>$poyecto,
                'statusCode'=>'exito',
                'proyectoPago'=>$proyectoPago,
                'cuota'=>$proyectoPago[0]->proyectoPgo->count()
            ];
            }
        }
        return $poyecto;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request['data'], [
            'nombre' => 'required|unique:proyectos' ,
            'monto' => 'required|numeric',
            'cuotas' => 'required|numeric'
        ]);
        $user=Auth::user();
        $correo = $user->email;
        if(count($validator->messages())>0){
            $poyecto = [
                'message'=>$validator->messages(),
                'statusCode'=>'error'
            ];
        }else{
            $data=[
                'nombre'=>$user->name,
                'correo'=>$user->correo,
            ];
            /*Mail::queue('correo.register',['data'=>$data],function($msj) use ($correo){
                $msj->subject('Registro exitoso');
                $msj->to($correo);
            });*/
            dispatch(new SendVerificationEmail($user));
            $poyecto = new Proyecto($request['data']);
            $poyecto->save();
            $poyecto = [
                'message'=>$poyecto,
                'statusCode'=>'exito'
            ];
        }
        return $poyecto;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proyecto = Proyecto::find($id);
        $proyectoPago = Proyecto::select('proyectos.id','proyectos.nombre')->where('proyectos.id',$id)->with('proyectoPgo')->get();
        return view("proyecto.formViewProyecto",['proyecto'=>$proyecto,'proyectoPago'=>$proyectoPago]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $proyecto = Proyecto::find($id);
        return view("proyecto.formEditProyecto",['proyecto'=>$proyecto]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request['data'], [
            'nombre' => 'required' ,
            'monto' => 'required|numeric',
            'cuotas' => 'required|numeric'
        ]);
        if(count($validator->messages())>0){
            $poyecto = [
                'message'=>$validator->messages(),
                'statusCode'=>'error'
            ];
        }else{
            $poyecto = Proyecto::find($id);
            $poyecto->nombre = $request['data']['nombre'];
            $poyecto->monto = $request['data']['monto'];
            $poyecto->cuotas = $request['data']['cuotas'];
            $poyecto->save();
            $poyecto = [
                'message'=>$poyecto,
                'statusCode'=>'exito'
            ];
        }
        return $poyecto;
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->deleted_at=date('Y-m-d H:i:s');
        $proyecto->save();
        $proyecto = Proyecto::find($id);
        return $proyecto;
        //
    }

    public function reactivar($id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->deleted_at=null;
        $proyecto->save();
        $proyecto = Proyecto::find($id);
        return $proyecto;
        //
    }
}
