<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProyectoPago;

class Proyecto extends Model
{


	protected $fillable = [
    	'nombre', 'monto', 'cuotas',
    ];


    public function proyectoPgo()
    {
        return $this->hasMany(ProyectoPago::class,'proyecto_id');
    }
    //
}
