<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\EmailVerification;
use Auth;

class SendVerificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user=Auth::user();
        $correo = Auth::user()->email;
        $email = new EmailVerification($user);
        //\Mail::to($correo)->send($correo);
        \Mail::send('correo.register',['data'=>$user],function($msj) use ($correo){
                $msj->subject('Registro exitoso');
                $msj->to($correo);
            });
    }
}
